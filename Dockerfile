FROM debian:buster
  
LABEL maintainer="lqb <lqb@gmx.de>"

RUN apt update \
 && apt install -y \
      bash \
      reprepro \
      git \
      git-lfs \
      curl \
      wget \
      unzip \
      openssh-client \
 && rm -rf \
      /var/lib/apt/lists

